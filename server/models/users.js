const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Users = new Schema ({
    _id: mongoose.Schema.Types.ObjectId,
    fname: { type: String, required: true },
    lname: { type: String, required: true },
    age: { type: Number, required: true },
    isAdmin: {type: Boolean, required: true}
});

module.exports = mongoose.model('Users', Users)