const MongoService = require("./mongo.service");

class UserService{

    constructor() {
        this.userTable = MongoService.collections.users;
        if(!this.userTable){
            throw new Error("Collection user does not exist!");
        }
    }

    async getAllUsers(){
        return await this.userTable.find({}).toArray();
    }

    async setUser(user) {
        return await this.userTable.post(
            {fname: user.fname},
            {lname: user.lname},
            {age: user.age},
            {isAdmin: user.isAdmin},
            {$set: user},
            {upsert: true}
        );
    }

    async deleteUserByName(user) {
        return await this.userTable.deleteOne({name: user.name});
    }
}

module.exports = UserService;