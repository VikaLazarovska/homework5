async function GetUsers() {

    const response = await fetch("/api/users", {
        method: "GET",
        headers: { "Accept": "application/json" }
    });

    if (response.ok === true) {

        const users = await response.json();
        let rows = document.querySelector("tbody");
        users.forEach(user => {

            rows.append(row(user));
        });
    }
}

async function tableSearch() {
    let phrase = document.getElementById('search-text');
    let table = document.getElementById('table');
    let regPhrase = new RegExp(phrase.value, 'i');
    let flag = false;
    for (let i = 1; i < table.rows.length; i++) {
        flag = false;
        for (let j = table.rows[i].cells.length - 1; j >= 0; j--) {
            flag = regPhrase.test(table.rows[i].cells[j].innerHTML);
            if (flag) break;
        }
        if (flag) {
            table.rows[i].style.display = "";
        } else {
            table.rows[i].style.display = "none";
        }

    }
}

async function GetUser(id) {
    const response = await fetch("/api/users/" + id, {
        method: "GET",
        headers: { "Accept": "application/json" }
    });
    if (response.ok === true) {
        const user = await response.json();
        const form = document.forms["userForm"];
        form.elements["id"].value = user._id;
        form.elements["name"].value = user.name;
        form.elements["surname"].value = user.surname;
        form.elements["age"].value = user.age;
    }
}

async function CreateUser(userName, userSurname, userAge) {

    const response = await fetch("api/users", {
        method: "POST",
        headers: { "Accept": "application/json", "Content-Type": "application/json" },
        body: JSON.stringify({
            name: userName,
            surname: userSurname,
            age: parseInt(userAge, 10),
            isAdmin: false
        })
    });
    if (response.ok === true) {
        const user = await response.json();
        document.querySelector("tbody").append(row(user));
    }
}

async function EditUser(userId, userName, userSurname, userAge) {
    const response = await fetch("api/users", {
        method: "PUT",
        headers: { "Accept": "application/json", "Content-Type": "application/json" },
        body: JSON.stringify({
            id: userId,
            name: userName,
            surname: userSurname,
            age: parseInt(userAge, 10)
        })
    });
    if (response.ok === true) {
        const user = await response.json();

        document.querySelector("tr[data-rowid='" + user._id + "']").replaceWith(row(user));
    }
}

async function DeleteUser(id) {
    const response = await fetch("/api/users/" + id, {
        method: "DELETE",
        headers: { "Accept": "application/json" }
    });
    if (response.ok === true) {
        const user = await response.json();
        document.querySelector("tr[data-rowid='" + user._id + "']").remove();
    }
}




function row(user) {

    const tr = document.createElement("tr");
    tr.setAttribute("data-rowid", user._id);

    const idTd = document.createElement("td");
    idTd.append(user._id);
    tr.append(idTd);

    const nameTd = document.createElement("td");
    nameTd.append(user.name);
    tr.append(nameTd);

    const surnameTd = document.createElement("td");
    surnameTd.append(user.surname);
    tr.append(surnameTd);

    const ageTd = document.createElement("td");
    ageTd.append(user.age);
    tr.append(ageTd);

    const adminTd = document.createElement("td");
    adminTd.append(user.isAdmin);
    tr.append(adminTd);

    const linksTd = document.createElement("td");



    const removeLink = document.createElement("a");
    removeLink.setAttribute("data-id", user._id);
    removeLink.setAttribute("style", "cursor:pointer;padding:15px;");
    removeLink.append("Delete");
    removeLink.addEventListener("click", e => {

        e.preventDefault();
        DeleteUser(user._id);
    });

    linksTd.append(removeLink);
    tr.appendChild(linksTd);

    return tr;
}




document.forms["userForm"].addEventListener("submit", e => {
    e.preventDefault();
    const form = document.forms["userForm"];
    const id = form.elements["id"].value;
    const name = form.elements["name"].value;
    const surname = form.elements["surname"].value;
    const age = form.elements["age"].value;
    if (id == 0)
        CreateUser(name, surname, age);
    else
        EditUser(id, name, surname, age);
});

GetUsers();